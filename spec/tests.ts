import { assert, expect, test } from 'vitest'
import { schema } from '../src/store/validate.ts'
import { defaultData } from '../src/store/types'

test('defaultData() match schema', () => {
    schema.parse(defaultData())
})
