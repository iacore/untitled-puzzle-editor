import { all, bind, eq, either, any, both, succeed, fail } from "kanrens/goal.js"
export { all, bind, eq, either, any, both, succeed, fail }

import State from 'kanrens/state.js';
import Pair from 'kanrens/pair.js'

# let BOARD_SIZE = 10

# let time = 0
# let ents = [
# 	{ tag: 'player', pos: {x:4, y:6}, facing: {x:0, y:1}, moved_at: -1 },
# 	{ tag: 'box', pos: {x:4, y:6}, facing: {x:0, y:0}, moved_at: -1 },
# ]

def pair_to_v2 pair
	if pair isa Pair
		{x:pair.left, y:pair.right}
	else
		pair

export class SearchState
	state = new State!
	i = 0
	goal = succeed!

	def oneOf entities
		let [state_, [v_pos_x, v_pos_y, v_tag]] = state.createVars(`{i}_pos_x`, `{i}_pos_y`, `{i}_tag`);
		i += 1
		state = state_

		let rest = entities.map do(ent)
			all
				eq v_tag, ent.tag
				eq v_pos_x, ent.pos.x
				eq v_pos_y, ent.pos.y
			
		goal = both(any(...rest), goal)

		{
			tag: v_tag
			pos: new Pair v_pos_x, v_pos_y
		}
	
	def get s, ent
		{
			pos: pair_to_v2(s.walk(ent.pos))
			tag: s.walk(ent.tag)
		}

	def solve extra_goal
		both(goal, extra_goal).pursue(state)


# let state = new SearchState!

# let e0 = state.oneOf(ents)
# let e1 = state.oneOf(ents)

# let goal = all(
# 	eq(e0.pos, e1.pos),
# 	eq(e0.tag, 'player'),
# 	eq(e1.tag, 'box'),
# )

# for r of state.solve(goal)
# 	console.log state.get(r, e0), state.get(r, e1)
