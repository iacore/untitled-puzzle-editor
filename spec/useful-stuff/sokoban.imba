import { State, Variable, goal as G } from 'kanrens'
const { eq, all, any, both, either, succeed } = G
import _ from 'lodash'

let BOARD_SIZE = 10

let Time = 0
let Entities = [
	{ id: 0, tag: 'player', pos: {x:5, y:6}, facing: {x:0, y:1}, moved_at: -1 }
	{ id: 1, tag: 'box', pos: {x:4, y:6}, facing: {x:0, y:0}, moved_at: -1 }
]

def get_entity id
	let e = Entities.filter(do(e) e.id == id)[0]
	unless e
		throw `Can't find entity with id {id}`
	e

# make sure those can be compired with `==` in JS
# see urls below for what 'path' is
# https://lodash.dev/docs/#get
# https://lodash.dev/docs/#set
const ENT_PATHS = [
	'id'
	'tag'
	'pos.x'
	'pos.y'
	'facing.x'
	'facing.y'
	'moved_at'
]

tag App
	<self[d:vflex]>
		for y in [0 ... BOARD_SIZE]
			<div[d:flex h:10]>
				for x in [0 ... BOARD_SIZE]
					<div[w:10 bd:solid 1px black]>
						for ent in Entities
							if ent.pos.x == x and ent.pos.y == y
								<span> ent.tag
								<br>

def infer
	let state = new State!
	let goals_ent = succeed!

	def declEntVar prefix
		def V name
			let newvar = new Variable `{prefix}_{name}`
			state._variables.push newvar
			newvar
		let v = {}
		for path in ENT_PATHS
			_.set(v, path, V path)

		def goal_is_ent ent
			all(...ENT_PATHS.map do(path)
				eq _.get(ent, path), _.get(v, path)
			)

		goals_ent = both(any(...(Entities.map goal_is_ent)), goals_ent)
		
		v

	def decode search_result, v
		let o = {}
		for path in ENT_PATHS
			_.set(o, path, search_result.walk _.get(v, path))
		o

	# declare symbolic entity
	let e0 = declEntVar('player') # the string 'player' here doesn't matter. it's for debugging
	let e1 = declEntVar('box')

	# the rule
	let goal_custom = all(
		eq(e0.tag, 'player'),
		eq(e1.tag, 'box'),
		eq(e0.moved_at, Time)
		eq(e0.pos.x, e1.pos.x),
		eq(e0.pos.y, e1.pos.y),
	)

	for s of both(goals_ent, goal_custom).pursue(state)
		let player = decode(s, e0) # now you get actual entity (or symbolic variables, meaning that value can be anything)
		let box = decode(s, e1) # this is a copy of the actual object, mutating this will not change game state
		console.log(box, Entities)
		let real_box = get_entity box.id # get the actual object by reference
		real_box.pos.x += player.facing.x
		real_box.pos.y += player.facing.y


if true
	window.addEventListener 'keydown', do(e)
		# on key press, move player and set facing
		let player = Entities[0]
		switch e.key
			when 'ArrowUp'
				player.facing = { x: 0, y: -1 }
			when 'ArrowDown'
				player.facing = { x: 0, y: +1 }
			when 'ArrowLeft'
				player.facing = { x: -1, y: 0 }
			when 'ArrowRight'
				player.facing = { x: +1, y: 0 }
			else
				return
		Time += 1
		player.pos.x += player.facing.x
		player.pos.y += player.facing.y
		player.moved_at = Time
		infer!
		imba.commit! # update ui

	imba.mount <App>
