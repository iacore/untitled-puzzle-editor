type Val = number | string | undefined;
type Goal<T extends Val[]> = {

}

export function unify<V extends Val, T extends Val[]>(left: V, right: V): Goal<T> {
    return {}
}

export function disj<T extends Val[]>(goals: Goal<T>[]): Goal<T> {
    return {}
}

export function conj<T extends Val[]>(goals: Goal<T>[]): Goal<T> {
    return {}
}

export function fresh<T extends Val[], V extends Val>(name: any, f: (val: V) => Goal<[...T, V]>): Goal<[...T, V]> {
    return {}
}

// runAll, run