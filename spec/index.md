## Directory structure

`src`: editor source code.
`spec/useful-stuff`: stuff useful to development, but not integrated yet

## Editor tabs

- [ ] map/level editor (with topology/mapping and unlock-logic scripting)
- [ ] entity+rule editor (scripting)
    - animation
    - rule editor
    - cosmetic programming
- [x] sprite editor
- [ ] sound effects editor (bfxr)
- (optional?) music editor
- [ ] key binder
- [ ] option

## Logic presets
built-in simple logic scripts
- move avatar on arrow keys

## Serialization
should be in yaml, or some better format
some data type (like tables) require special formatting

- localStorage
- locally, as a plain file

## Universal settings

Keybinds should persist across different games, or web domains. Some other settings like language preference should too.
