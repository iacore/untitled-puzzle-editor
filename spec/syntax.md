## example sokoban game

```
global
    time: int

entity _base
    id: string
    pos: vec2
    facing: vec2
    sprite = ...
    tag = "..."

onkeydown "ArrowUp"
    global.time += 1
    for {$a: player} in queryEntities(do($a) [$a.tag=player])
        player.facing = (0, -1)
        player.pos += player.facing
    
    for {$a} in queryEntities(do($a, $b) [$a.pos=$b.pos, $a.tag=player, $b.tag=box])
        $b.pos += $a.facing
```
