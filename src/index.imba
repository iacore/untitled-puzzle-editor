import { Draggable } from './utils'
import { loadData, saveData, clearData } from './store/persist'
import { raiseStackHelper } from './windows'

global.L = console.log

global.ALERTS = {}
global.Alert = do(id, text)
	ALERTS[id] = text
	imba.commit!

global.Unalert = do(id)
	delete ALERTS[id]
	imba.commit!

tag Nav
	prop data\Record<string,WindowConfig>
	prop stacking

	size=9em

	<self [pos:absolute top:.5em left:.5em]>
		<Draggable bind=data.root>
			<nav>
				css m:auto d:vflex jc:center g:.2 size:{size}
					rd:full bg:gray3 bd:solid 1px black
				css >div d:flex jc:center g:2
				<div>
					<WindowToggle bind=data.lvl name='lvl' stacking=stacking>
					<WindowToggle bind=data.ent name='ent' stacking=stacking>
				<div>
					<WindowToggle bind=data.rul name='rul' stacking=stacking>
					<WindowToggle bind=data.cfg name='cfg' stacking=stacking>
					<WindowToggle bind=data.kbd name='kbd' stacking=stacking>
				<div>
					<WindowToggle bind=data.spr name='spr' stacking=stacking>
					<WindowToggle bind=data.sfx name='sfx' stacking=stacking>

tag WindowToggle
	prop data
	prop stacking
	prop name\string

	def _toggle
		data.open = !data.open
		if data.open
			raiseStackHelper stacking, name


	<self [pos:relative]>
		<button .on=data.open @click=_toggle> name.toUpperCase()
			css cursor:pointer bg.on:white .on@active:gray2 bd:solid bw:1px bc:black .on:transparent s:40px rd:full d:flex ai:center jc:center
				tween:all 0.2s

tag Root
	# prop data

	def setup
		global.getData = do data

		let savecb = do(event)
			try
				saveData data
				Unalert 'validate'
			catch err
				const errstring =
					"You have fed us some invalid data. \n" +
					"Your progress will not be saved if you don't fix it first.\n\n" +
					err
				Alert 'validate', errstring
				if event
					event.preventDefault!
					return event.returnValue = errstring
	
		let h = setInterval savecb, 5000
		let h2 = window.addEventListener 'beforeunload', savecb, {capture: true}
		
		global.clearData = do
			clearData!
			window.removeEventListener 'beforeunload', savecb, {capture: true}
			window.location.reload!

		data = loadData!

	global css body of:hidden

	<self [w:100vw h:100vh bg:gray4]>
		css $game-bgc:{data.global.bgc}
		<Nav [zi:0] bind=data.window stacking=data.stacking>
		<.windows [isolation:isolate]>
			<w-frame bind=data name='lvl'>
				<w-entity-editor>
			<w-frame bind=data name='ent'>
				<w-level-editor>
			<w-frame bind=data name='rul'>
				<w-rule-editor>
			<w-frame bind=data name='cfg'>
				<w-settings bind=data.global>
			<w-frame bind=data name='kbd'>
				<w-keybinds>
			<w-frame bind=data name='spr'>
				<w-sprite-editor bind=data.sprite>
			<w-frame bind=data name='sfx'>
				<w-sfx-editor>
		<[d:vflex g:2 isolation:isolate zi:20]>
			for [alert_name, msg] in Object.entries global.ALERTS
				<div[bg:orange4 p:2 ws:pre-wrap] key=alert_name> msg
					<button[td:underline bg:white float:right] href="#" @click=(Unalert alert_name)> "Close"

imba.mount <Root>
