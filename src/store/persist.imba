
const localStorageKey = 'global-v0'

import { schema } from './validate'
import { defaultData } from './types'

export def saveData data
	try
		const sanitized = schema.parse(data)
		localStorage.setItem localStorageKey, JSON.stringify(sanitized)
	catch error
		console.error('Save data has invalid shape', data)
		throw error

def loadData_untrusted
	const dataString = localStorage.getItem localStorageKey
	if dataString
		try
			return JSON.parse(dataString)
		catch e
			console.error('load data failed ', e)
	defaultData!

export def loadData
	const data = loadData_untrusted!

	try
		schema.parse(data)
	catch error
		console.error('Load data has invalid shape', data)
		// todo: make user decide whether to clear or use another save
		throw error

export def clearData
	localStorage.removeItem(localStorageKey)
