import { z } from "zod";
import colorMap from "./common_colors.js";
// import { defaultData } from "./types.imba";

const RGBA_REGEX =
  /#(([0-9A-Fa-f]{3,4}\b)|([0-9A-Fa-f]{6}\b)|([0-9A-Fa-f]{8})\b)/;
const html_color = z.string().refine(
  (val: string) => {
    if (val in colorMap) return true;

    return val.match(RGBA_REGEX);
  },
  {
    message: "Not a HTML color",
  }
);

const WINDOW_KEYS = ["root", "ent", "lvl", "rul", "cfg", "kbd", "spr", "sfx"];

const window_state = z.object({
  x: z.number(),
  y: z.number(),
  open: z.boolean(),
  name: z.string(),
});

const nanoid = z.string();

const sprite_id = nanoid;
const sprite_data = z.object({
  id: sprite_id,
  name: z.string(),
  paths: z.array(
    z.object({
      color: html_color,
      weight: z.number(),
      data: z.string(), // todo validate svg ptah
    })
  ),
});

const entity_id = nanoid;
const entity_field = z.object({
  default: z.string(),
});
const entity_data = z.object({
  id: entity_id,
  name: z.string(),
  // animation frames
  frames: z.array(sprite_id), // todo: check sprite_id are valid
  // object properties
  fields: z.record(z.string(), entity_field)
});

export const schema = z.object({
  window: z.object(
    Object.fromEntries(WINDOW_KEYS.map((k) => [k, window_state]))
  ),
  stacking: z.array(
    z
      .string()
      .refine((val) => WINDOW_KEYS.indexOf(val) != -1, {
        message: "not recognized window name",
      })
  ),
  sprite: z.array(sprite_data),
  global: z.object({
    bgc: html_color,
  }),
  entity: z.array(entity_data),
});

export type SaveData = z.infer<typeof schema>;
