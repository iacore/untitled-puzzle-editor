import { nanoid } from 'nanoid'

export def defaultData
	def default-window-status name
		x:0,y:0,open:no,name:name

	{
		window: {
			root: default-window-status! "window toggles"
			ent:  default-window-status! "Entity Editor"
			lvl:  default-window-status! "Level Editor"
			rul:  default-window-status! "Rule Editor"
			cfg:  default-window-status! "Settings"
			kbd:  default-window-status! "Keybinds"
			spr:  default-window-status! "Sprite Editor"
			sfx:  default-window-status! "Sound Effect Editor"
		}
		stacking: []
		sprite: []
		entity: []
		global:
			bgc: '#ffffff' # game's bg color
	}

export def newSprite
	let id = nanoid!
	
	{
		id: id
		name: "(untitled)"
		paths: []
	}
