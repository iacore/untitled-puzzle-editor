import { Draggable } from './utils'
import { newSprite } from './store/types'

export def raiseStackHelper stacking, name
	let cur = stacking.indexOf name
	if cur >= 0
		stacking.splice cur,1
	stacking.push name

tag w-frame
	prop data
	prop name\string

	def raiseStack
		raiseStackHelper data.stacking, name

	get stack
		data.stacking.indexOf name

	get win
		data.window[name]

	<self[zi:{stack} pos:relative] [d:none]=!win.open @touch=raiseStack>
		<Draggable [pos:absolute] bind=win>
			<.window[bg:white bd:solid 1px black isolation:isolate]>
				<.title[d:flex cursor:move]>
					<span[px:2 flg:1]> win.name
					<button bind=win.open [bg:unset m:unset bd:unset ol:solid 1px black va:top]> "❌"
				<.content>
					<slot>

tag w-entity-editor
	<self> "TODO"

tag w-level-editor
	<self> "TODO"

tag w-rule-editor
	<self> "TODO"

tag w-settings
	<self>
		<div>
			<label for="game-bgc"> "Bg Color"
			<input id="game-bgc" bind=data.bgc>
		"TODO"

tag w-keybinds
	<self> "TODO"

import './canvas'

tag w-sprite-editor
	prop data
	i = 0

	def createSprite
		data.push newSprite!
		i = data.length - 1
	
	def deleteSprite
		data.splice i,1
		while i >= data.length
			i--

	<self>
		if data[i]
			<active-sprite-editor bind=data[i] on_delete=(do deleteSprite!)>
				<[d:vflex g:1]>
					<div>
						<label> "Name: "
						<input bind=data[i].name>
					<[d:vgrid g:1 ai:flex-start gtc:repeat(10, 1fr)]>
						for sprite, ii in data
							<app-canvas [bc:gray2]=(i!=ii) key=sprite.id _size=32 data=sprite disabled @click=(i=ii)>
						<button[s:32px fs:xl lh:1] @click=createSprite> "+"
		else
			<button @click=createSprite> "Create sprite"

tag w-sfx-editor
	<self> "TODO"
