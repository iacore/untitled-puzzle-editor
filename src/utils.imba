export tag Draggable
	prop data = { x:0,y:0 }
	
	prop h2

	def mount
		clip!
		h2 = do(e)
			window.requestAnimationFrame do
				clip!
		window.addEventListener 'resize', h2

	def unmount
		window.removeEventListener 'resize', h2

	def clip
		const me = self.getBoundingClientRect()
		const inside = document.body.getBoundingClientRect()
		let d
		if (d = me.top - inside.top) < 0
			data.y -= d
		if (d = me.bottom - inside.bottom) > 0
			data.y -= d
		if (d = me.left - inside.left) < 0
			data.x -= d
		if (d = me.right - inside.right) > 0
			data.x -= d
		imba.commit!
	
	def ontouch e
		if e.phase == 'ended'
			clip!

	<self @touch.moved.sync(data)=ontouch [x:{data.x}px y={data.y}px isolation:isolate]>
		<slot>
