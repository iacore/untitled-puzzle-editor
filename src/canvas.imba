# Adopted from the draw demo on https://imba.io/
# Don't ask me how this works. It just do.

const STROKES = [5,3,2,1]
const COLORS = [ # https://lospec.com/palette-list/dawnbringer-16
	'#140c1c'
	'#442434'
	'#30346d'
	'#4e4a4e'
	'#854c30'
	'#346524'
	'#d04648'
	'#757161'
	'#597dce'
	'#d27d2c'
	'#8595a1'
	'#6daa2c'
	'#d2aa99'
	'#6dc2ca'
	'#dad45e'
	'#deeed6'
]

const LARGE_CANVAS_SIZE = 512;
const SMALL_CANVAS_SIZE = 64;
const SCALE = LARGE_CANVAS_SIZE / SMALL_CANVAS_SIZE;

tag active-sprite-editor
	prop data\_schema_sprite
	prop on_delete
	state = {stroke: STROKES[2], color: COLORS[0]}
	
	css button bd:solid rd:lg bg:white cursor:pointer @disabled:unset fs:lg

	<self[of:hidden $scale:{SCALE} px:1]>
		<[d:flex g:1 my:1]>
			# <[d:vgrid g:1]>
			<[d:vflex ai:stretch jc:flex-end g:1 ta:center w:{SMALL_CANVAS_SIZE}px]>
				<app-canvas _size=SMALL_CANVAS_SIZE data=data disabled>
				<[fs:sm mb:1 c:cool5]> "preview"
				if data.paths.length
					<button @click=data.paths=[]> "clear"
				else
					<button[bc:red6] @click=on_delete disabled=!on_delete> "delete"
				<button disabled=!data.paths.length @click=data.paths.pop!> "undo"
			<slot>
		<[pos:relative w:{LARGE_CANVAS_SIZE}px]>
			<app-canvas _size=LARGE_CANVAS_SIZE state=state bind=data>
			if data.paths && data.paths.length == 0
				<[ta:center pt:20 o:0.2 fs:xl pe:none pos:absolute t:0 w:100% c:black]> 'draw here'
		<.tools [d:hflex ja:space-around]>
			<[flg:1 d:flex ja:center]>
				<StrokePicker options=STROKES bind=state.stroke>
			<[flg:1 d:flex ja:center end]>
				<ColorPicker options=COLORS bind=state.color>

/** Paintable vector canvas */
tag app-canvas
	prop _size = 512
	prop state = {}
	prop disabled = false
	prop data

	def awaken
		refresh!
	
	def rendered
		refresh!

	def draw e
		if e.#path == undefined
			e.#path = "M{e.x / _size},{e.y / _size}"
			e.#id = data.paths.length
		else
			e.#path += " L{e.x / _size},{e.y / _size}"

		data.paths[e.#id] =
			color: state.color
			weight: state.stroke
			data: e.#path
		# refresh!

	def refresh
		# L data
		let ctx = $canvas.getContext('2d')
		ctx.resetTransform()
		ctx.scale(_size, _size)
		ctx.clearRect(0, 0, $canvas.width, $canvas.height)
		for p in data.paths
			ctx.lineWidth = p.weight / SMALL_CANVAS_SIZE
			ctx.strokeStyle = p.color
			ctx.stroke(new Path2D(p.data))

	<self[bd:solid 1px black bg:$game-bgc w:{_size}px h:{_size}px]>
		<canvas$canvas width=_size height=_size @touch.stop.fit.if(!disabled)=draw>

tag ValuePicker
	css .item h:100% tween:styles 0.1s ease-out

	<self[h:40px d:hflex]>
		for item in options
			<div.item-c[px:$gap] @click=(data = item)>
				<div.item[$value:{item}] .sel=(item==data)>

tag StrokePicker < ValuePicker
	css py:1 $gap:2px
	css .item-c@hover .item o:0.8
	css .item bg:black w:calc($value*$scale*1px)
		o:0.3 .sel:1

tag ColorPicker < ValuePicker
	css pt:1 $gap:1px
	css .item-c mb:-10px
		&@hover .item outline:solid 1px black
	css .item js:stretch rdt:xl bg:$value y.sel:-10px w:5
